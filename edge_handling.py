import numpy as np
import cv2


def convolve_crop(image, kernel):
    output = np.zeros((image.shape[0] - kernel.shape[0] + 1,
                       image.shape[1] - kernel.shape[1] + 1))

    for i in range(0, output.shape[0]):
        for j in range(0, output.shape[1]):
            window = image[i: i + kernel.shape[0], j: j + kernel.shape[1]]
            output[i, j] = np.sum(np.multiply(kernel, window))

    output[output > 255] = 255
    output[output < 0] = 0
    output = output.astype(np.uint8)
    return output


def convolve_extend(image, kernel):
    output = np.zeros((image.shape[0], image.shape[1]))

    # image preproccesing - extending the image
    border_a = kernel.shape[1] - 1
    border_b = kernel.shape[0] - 1
    img_extended = cv2.copyMakeBorder(
        image,
        top=border_a,
        bottom=border_a,
        left=border_b,
        right=border_b,
        borderType=cv2.BORDER_REPLICATE)

    # convolution
    for i in range(0, output.shape[0]):
        for j in range(0, output.shape[1]):
            window = img_extended[i: i + kernel.shape[0], j: j + kernel.shape[1]]
            output[i, j] = np.sum(np.multiply(kernel, window))

    output[output > 255] = 255
    output[output < 0] = 0
    output = output.astype(np.uint8)
    return output


def convolve_mirror(image, kernel):
    output = np.zeros((image.shape[0], image.shape[1]))

    # image preproccesing - extending the image
    border_a = kernel.shape[1] - 1
    border_b = kernel.shape[0] - 1
    img_extended = cv2.copyMakeBorder(
        image,
        top=border_a,
        bottom=border_a,
        left=border_b,
        right=border_b,
        borderType=cv2.BORDER_REFLECT)

    # convolution
    for i in range(0, output.shape[0]):
        for j in range(0, output.shape[1]):
            window = img_extended[i: i + kernel.shape[0], j: j + kernel.shape[1]]
            output[i, j] = np.sum(np.multiply(kernel, window))

    output[output > 255] = 255
    output[output < 0] = 0
    output = output.astype(np.uint8)
    return output


def convolve_wrap(image, kernel):
    output = np.zeros((image.shape[0], image.shape[1]))

    # image preproccesing - extending the image
    border_a = kernel.shape[1] - 1
    border_b = kernel.shape[0] - 1
    img_extended = cv2.copyMakeBorder(
        image,
        top=border_a,
        bottom=border_a,
        left=border_b,
        right=border_b,
        borderType=cv2.BORDER_WRAP)

    # convolution
    for i in range(0, output.shape[0]):
        for j in range(0, output.shape[1]):
            window = img_extended[i: i + kernel.shape[0], j: j + kernel.shape[1]]
            output[i, j] = np.sum(np.multiply(kernel, window))

    output[output > 255] = 255
    output[output < 0] = 0
    output = output.astype(np.uint8)
    return output


# kernels
identity = np.array([[0, 0, 0], [0, 1, 0], [0, 0, 0]])
edge_det = np.array([[-1, -1, -1], [-1, 8, -1], [-1, -1, -1]])
sharpen = np.array([[0, -1, 0], [-1, 5, -1], [0, -1, 0]])
box_blur = np.array([[1/9, 1/9, 1/9], [1/9, 1/9, 1/9], [1/9, 1/9, 1/9]])


img = cv2.imread(r"C:\Users\Korisnik\Desktop\osirv_projekt\source\baboon.bmp", cv2.IMREAD_GRAYSCALE)

# identity
cv2.imwrite("identity_crop.jpg", convolve_crop(img, identity))
cv2.imwrite("identity_extend.jpg", convolve_extend(img, identity))
cv2.imwrite("identity_mirror.jpg", convolve_mirror(img, identity))
cv2.imwrite("identity_wrap.jpg", convolve_wrap(img, identity))

# edge detetcion
cv2.imwrite("edge_det_crop.jpg", convolve_crop(img, edge_det))
cv2.imwrite("edge_det_extend.jpg", convolve_extend(img, edge_det))
cv2.imwrite("edge_det_mirror.jpg", convolve_mirror(img, edge_det))
cv2.imwrite("edge_det_wrap.jpg", convolve_wrap(img, edge_det))

# sharpen
cv2.imwrite("sharpen_crop.jpg", convolve_crop(img, sharpen))
cv2.imwrite("sharpen_extend.jpg", convolve_extend(img, sharpen))
cv2.imwrite("sharpen_mirror.jpg", convolve_mirror(img, sharpen))
cv2.imwrite("sharpen_wrap.jpg", convolve_wrap(img, sharpen))

# box blur
cv2.imwrite("blur_crop.jpg", convolve_crop(img, box_blur))
cv2.imwrite("blur_extend.jpg", convolve_extend(img, box_blur))
cv2.imwrite("blur_mirror.jpg", convolve_mirror(img, box_blur))
cv2.imwrite("blur_wrap.jpg", convolve_wrap(img, box_blur))