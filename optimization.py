import numpy as np
from skimage.util.shape import view_as_windows
import cv2

def im2col(array, kernel):
    rows = []

    for row in range(array.shape[0] - kernel.shape[0] + 1):
        for col in range(array.shape[1] - kernel.shape[1] + 1):
            window = array[row: row + kernel.shape[0], col: col + kernel.shape[1]]
            rows.append(window.flatten())

    return np.transpose(np.array(rows))


def im2col_conv(array, kernel):

    im2col_array = im2col(array, kernel)
    result = np.dot(kernel.flatten(), im2col_array)

    return result.reshape(array.shape[0] - kernel.shape[0] + 1, array.shape[1] - kernel.shape[1] + 1)


def memory_strided_im2col(array, kernel):

    windows = view_as_windows(array, kernel.shape)
    return windows.reshape((array.shape[0] - kernel.shape[0] + 1)*(array.shape[1] - kernel.shape[1] + 1), kernel.shape[0]*kernel.shape[1])


def memory_strided_im2col_conv(array, kernel):
    
    mem_str_array = memory_strided_im2col(array, kernel)
    result = np.dot(mem_str_array, kernel.flatten())
    return result.reshape(array.shape[0] - kernel.shape[0] + 1, array.shape[1] - kernel.shape[1] + 1)


# kernels
identity = np.array([[0, 0, 0], [0, 1, 0], [0, 0, 0]])
edge_det = np.array([[-1, -1, -1], [-1, 8, -1], [-1, -1, -1]])
sharpen = np.array([[0, -1, 0], [-1, 5, -1], [0, -1, 0]])
box_blur = np.array([[1/9, 1/9, 1/9], [1/9, 1/9, 1/9], [1/9, 1/9, 1/9]])


img = cv2.imread(r"C:\Users\Korisnik\Desktop\osirv_projekt\source\baboon.bmp", cv2.IMREAD_GRAYSCALE)

# identity
cv2.imwrite("identity_im2col.jpg", im2col_conv(img, identity))
cv2.imwrite("identity_mem_str.jpg", memory_strided_im2col_conv(img, identity))

# edge detetcion
cv2.imwrite("edge_det_im2col.jpg", im2col_conv(img, edge_det))
cv2.imwrite("edge_det_mem_str.jpg", memory_strided_im2col_conv(img, edge_det))

# sharpen
cv2.imwrite("sharpen_im2col.jpg", im2col_conv(img, sharpen))
cv2.imwrite("sharpen_mem_str.jpg", memory_strided_im2col_conv(img, sharpen))

# box blur
cv2.imwrite("blur_im2col.jpg", im2col_conv(img, box_blur))
cv2.imwrite("blur_mem_str.jpg", memory_strided_im2col_conv(img, box_blur))