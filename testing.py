import cv2
import numpy as np
import matplotlib.pyplot as plt

from scipy import ndimage
from time import time

from edge_handling import convolve_crop
from optimization import im2col_conv, memory_strided_im2col_conv


# kernels
identity = np.array([[0, 0, 0], [0, 1, 0], [0, 0, 0]])
edge_det = np.array([[-1, -1, -1], [-1, 8, -1], [-1, -1, -1]])
sharpen = np.array([[0, -1, 0], [-1, 5, -1], [0, -1, 0]])
box_blur = np.array([[1/9, 1/9, 1/9], [1/9, 1/9, 1/9], [1/9, 1/9, 1/9]])


# performance testing
input_size_list = np.arange(5, 1601, 5)
runs = 320

basic_time_log = []
im2col_time_log = []
strided_time_log = []
cv2_time_log = []
scipy_time_log = []

for input_size in input_size_list:
    array = np.ones([input_size, input_size])

    start = time()
    convolve_crop(array, identity)
    basic_time = time() - start

    start = time()
    im2col_conv(array, identity)
    im2col_time = time() - start

    start = time()
    memory_strided_im2col_conv(array, identity)
    strided_time = time() - start

    start = time()
    cv2.filter2D(array, -1, identity)
    cv2_time = time() - start

    start = time()
    ndimage.convolve(array, identity, mode='constant', cval=0.0)
    scipy_time = time() - start

    basic_time_log.append(basic_time)
    im2col_time_log.append(im2col_time)
    strided_time_log.append(strided_time)
    cv2_time_log.append(cv2_time)
    scipy_time_log.append(scipy_time)

# plot
plt.plot(input_size_list, basic_time_log,
    label='basic convolve', color='black')
plt.plot(input_size_list, im2col_time_log,
    label='im2col', color='green')
plt.plot(input_size_list, strided_time_log,
    label='mem strided im2col', color='purple')
plt.plot(input_size_list, cv2_time_log,
    label='cv2.filter2D', color='orange')
plt.plot(input_size_list, scipy_time_log,
    label='scipy.convolve', color='magenta')
plt.xlabel('Size of Input (n x n)')
plt.ylabel('Execution Time (secs)')
plt.legend()
plt.show()